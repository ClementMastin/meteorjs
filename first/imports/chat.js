import {Meteor} from "meteor/meteor";
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import "./collection.js";


import "./chat.html";


import "./connection.js";
import './connection.html';
import { Messages } from "./collection.js";

Meteor.subscribe("Messages");

Template.list_messages.onCreated(() => {
	
	
	this.messages = null;
});

Template.list_messages.helpers({
	messages: function(){
		return Messages.find();
	},
});

Template.entree_messages.events({
	'submit form': function(event){
		event.preventDefault();
		var text = event.target.input_text.value;
		Meteor.call("new_messages",Session.get("userName"),text,function(e,r){
			
			
		});
	}
});



Template.deconnexion.events({
	"click #deco": function(event){
		event.preventDefault();
		Meteor.call("remove_user",Session.get("userName"));
		Session.set("userName",null);
	}
})