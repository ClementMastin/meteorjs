import "./connection.html";
import "./collection.js";

import "./chat.html";

import {Meteor} from "meteor/meteor";
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';


//Meteor.subscribe("Users");

/*Template.inscription.onCreated(function appBodyOnCreated(){
	this.subscribe("Users");
})*/

Template.inscription.events({
	'submit #cliquer': function(event,instance){
		
		event.preventDefault();
		const text = event.target.input_name.value;
		var r = Meteor.call("usersAdd",text,function(e,result){
			
			if(result){
				Session.set("userName",text);
				//userName = text;
			}
			else{
				alert("Nom déjà pris");
			}
		});
		
	},
})