# TP Chat
Dans ce TP, vous allez explorer les principales fonctionnalités de Meteor et créer une application de 
chat fonctionnelle .Ce TP comprendra 3 parties:
-Installez Meteor et créez votre application
-Implémenter un chat de base
-Ajouter des comptes d'utilisateurs

## Créer un projet
Tapez dans le terminal:
```bash
meteor create
meteor remove autopublish
meteor remove insecure
meteor npm install --save @babel/runtime
meteor
```

Cela créera un projet Meteor et le lancera sur votre machine. Le port
par défaut est 3000.

Un squelette de code HTML est défini dans client/main.html.Laissez seulement 
la partie head dans ce fichier et déplacez la partie body dans un nouveau
fichier du dossier import. Le reste du TP se fera dans le dossier import


##Initialisation

A chaque démarrage du serveur, faites en sorte de créer les BDD correspondantes
et de les vider

## Connexion

Dans le body du HTML, faites en sorte que si la session n'est pas active, une
page permettant de se connecter avec un login s'affiche. Pour réaliser cette
tâche, vous aurez besoin de l'objet Session. Pour pouvoir l'utiliser, lancez
dans le terminal: meteor add session. vous aurez aussi d'une BDD gérant les
utilisateurs. Bien évidemment, vous devez aussi gérer le cas où un autre utilisateur
à déjà pris le login que vous voulez

## Rendre les messages en HTML

En vous servant de la BDD des messages (il existe une façon simple
de le faire), affichez un par un les messages écrits par les différents utilisateurs.


## Deconnexion

Rajoutez un bouton "Deconnexion" permettant de se déconnecter et de revenir sur
la page de connexion.


